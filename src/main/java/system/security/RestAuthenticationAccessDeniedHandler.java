package system.security;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

/**
 * Created by Pawel Polit
 */

public class RestAuthenticationAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest p_request, HttpServletResponse p_response,
                       AccessDeniedException p_e) throws IOException, ServletException {
        p_response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");

    }
}