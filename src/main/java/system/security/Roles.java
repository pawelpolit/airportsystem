package system.security;

/**
 * Created by Pawel Polit
 */

public enum Roles {
    ROLE_ADMIN,
    ROLE_EMPLOYEE
}
