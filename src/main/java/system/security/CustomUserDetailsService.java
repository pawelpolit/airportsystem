package system.security;

import system.dao.UserLoginDataDAO;
import system.entity.UserLoginData;

import java.util.Collection;

import com.google.common.collect.Lists;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Created by Pawel Polit
 */

public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserLoginDataDAO userLoginDataDAO;

    @Override
    public UserDetails loadUserByUsername(String p_userName) throws UsernameNotFoundException {

        UserLoginData userLoginData = userLoginDataDAO.findByUsername(p_userName);

        if(userLoginData == null) {
            return null;
        }

        CustomUserData customUserData = new CustomUserData();
        customUserData.setAuthentication(true);
        customUserData.setPassword(userLoginData.getPassword());

        Collection<CustomRole> roles = Lists.newArrayList(new CustomRole().setAuthority(userLoginData.getRole()));

        if(Roles.ROLE_ADMIN.name().equals(userLoginData.getRole())) {
            roles.add(new CustomRole().setAuthority(Roles.ROLE_EMPLOYEE.name()));
        }

        customUserData.setAuthorities(roles);

        return customUserData;
    }

    private class CustomRole implements GrantedAuthority {
        private String role = null;

        @Override
        public String getAuthority() {
            return role;
        }

        public CustomRole setAuthority(String p_roleName) {
            role = p_roleName;
            return this;
        }

    }
}
