package system.security;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Created by Pawel Polit
 */

public class CustomUserData implements UserDetails {
    Collection<? extends GrantedAuthority> list = null;
    String userName = null;
    String password = null;
    boolean status = false;

    public CustomUserData() {
        list = new ArrayList<>();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return list;
    }

    public void setAuthorities(Collection<? extends GrantedAuthority> p_roles) {
        list = p_roles;
    }

    public void setAuthentication(boolean p_status) {
        status = p_status;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String p_password) {
        password = p_password;
    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}