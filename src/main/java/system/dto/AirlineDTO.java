package system.dto;

import system.entity.Airline;

/**
 * Created by Pawel Polit
 */

public class AirlineDTO {
    public Long id;
    public String name;

    public AirlineDTO() {
    }

    private AirlineDTO(Long p_id, String p_name) {
        id = p_id;
        name = p_name;
    }

    public static AirlineDTO create(Airline p_airline) {
        return new AirlineDTO(p_airline.getId(), p_airline.getName());
    }

    public Airline createEntityWithoutId() {
        return new Airline().setName(name);
    }

    public Airline createEntityWithId() {
        return createEntityWithoutId().setId(id);
    }
}
