package system.dto;

import system.entity.Flight;
import system.entity.Reservation;

/**
 * Created by Pawel Polit
 */

public class ReservationDTO {
    public Long id;
    public String name;
    public String surname;
    public Long flightId;

    public ReservationDTO() {
    }

    private ReservationDTO(Long p_id, String p_name, String p_surname, Long p_flightId) {
        id = p_id;
        name = p_name;
        surname = p_surname;
        flightId = p_flightId;
    }

    public static ReservationDTO create(Reservation p_reservation) {
        return new ReservationDTO(p_reservation.getId(),
                                  p_reservation.getPersonName(),
                                  p_reservation.getPersonSurname(),
                                  p_reservation.getFlight().getId());
    }

    public Reservation createEntityWithoutId(Flight p_flight) {
        return new Reservation()
                .setPersonName(name)
                .setPersonSurname(surname)
                .setFlight(p_flight);
    }

    public Reservation createEntityWithId(Flight p_flight) {
        return createEntityWithoutId(p_flight).setId(id);
    }
}
