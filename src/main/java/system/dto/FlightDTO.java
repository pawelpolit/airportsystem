package system.dto;

import system.entity.Flight;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

/**
 * Created by Pawel Polit
 */

public class FlightDTO {
    public Long id;
    public AirportDTO from;
    public AirportDTO to;
    public String dateOfDeparture;
    public Integer flightTime;
    public AirlineDTO airline;
    public Integer cost;
    public Integer maxNumberOfPassengers;

    public FlightDTO() {
    }

    private FlightDTO(Long p_id, AirportDTO p_from, AirportDTO p_to, String p_dateOfDeparture, Integer p_flightTime,
                      AirlineDTO p_airline, Integer p_cost, Integer p_maxNumberOfPassengers) {
        id = p_id;
        from = p_from;
        to = p_to;
        dateOfDeparture = p_dateOfDeparture;
        flightTime = p_flightTime;
        airline = p_airline;
        cost = p_cost;
        maxNumberOfPassengers = p_maxNumberOfPassengers;
    }

    public static FlightDTO create(Flight p_flight) {
        return new FlightDTO(p_flight.getId(),
                             AirportDTO.create(p_flight.getFromAirport()),
                             AirportDTO.create(p_flight.getToAirport()),
                             DateTimeFormat.forPattern("dd-MM-yyyy kk:mm").print(p_flight.getDateOfDeparture()),
                             p_flight.getFlightTime(),
                             AirlineDTO.create(p_flight.getAirline()),
                             p_flight.getFlightCost(),
                             p_flight.getMaxNumberOfPassengers());
    }

    public Flight createEntityWithoutId() {
        return new Flight().setFromAirport(from.createEntityWithId())
                .setToAirport(to.createEntityWithId())
                .setDateOfDeparture(DateTime.parse(dateOfDeparture, DateTimeFormat.forPattern("dd-MM-yyyy kk:mm")))
                .setFlightTime(flightTime)
                .setAirline(airline.createEntityWithId())
                .setFlightCost(cost)
                .setMaxNumberOfPassengers(maxNumberOfPassengers);
    }

    public Flight createEntityWithId() {
        return createEntityWithoutId().setId(id);
    }
}
