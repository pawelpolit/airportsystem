package system.dto;

import system.entity.Airport;

/**
 * Created by Pawel Polit
 */

public class AirportDTO {
    public Long id;
    public String city;

    public AirportDTO() {
    }

    private AirportDTO(Long p_id, String p_city) {
        id = p_id;
        city = p_city;
    }

    public static AirportDTO create(Airport p_airport) {
        return new AirportDTO(p_airport.getId(), p_airport.getCity());
    }

    public Airport createEntityWithoutId() {
        return new Airport().setCity(city);
    }

    public Airport createEntityWithId() {
        return createEntityWithoutId().setId(id);
    }
}
