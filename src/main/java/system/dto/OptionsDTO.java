package system.dto;

import java.net.URI;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * Created by Pawel Polit
 */

public class OptionsDTO {
    public Map<String, List<URI>> options;

    public OptionsDTO() {
        options = Maps.newTreeMap();
    }

    public static OptionsDTO create() {
        return new OptionsDTO();
    }

    public OptionsDTO addOption(String p_method, URI p_option) {

        if(!options.containsKey(p_method)) {
            options.put(p_method, Lists.newArrayList());
        }

        options.get(p_method).add(p_option);
        return this;
    }
}
