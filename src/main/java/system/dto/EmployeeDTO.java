package system.dto;

import system.entity.Employee;

/**
 * Created by Pawel Polit
 */

public class EmployeeDTO {
    public Long id;
    public String name;
    public String surname;
    public Integer age;
    public String pesel;
    public String position;

    public EmployeeDTO() {
    }

    private EmployeeDTO(Long p_id, String p_name, String p_surname, Integer p_age, String p_pesel, String p_position) {
        id = p_id;
        name = p_name;
        surname = p_surname;
        age = p_age;
        pesel = p_pesel;
        position = p_position;
    }

    public static EmployeeDTO create(Employee p_employee) {
        return new EmployeeDTO(p_employee.getId(),
                               p_employee.getName(),
                               p_employee.getSurname(),
                               p_employee.getAge(),
                               p_employee.getPesel(),
                               p_employee.getPosition());
    }

    public Employee createEntityWithoutId() {
        return new Employee()
                .setName(name)
                .setSurname(surname)
                .setAge(age)
                .setPesel(pesel)
                .setPosition(position);
    }

    public Employee createEntityWithId() {
        return createEntityWithoutId().setId(id);
    }
}
