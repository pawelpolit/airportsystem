package system.dto;

import system.entity.UserLoginData;
import system.security.Roles;

/**
 * Created by Pawel Polit
 */

public class UserLoginDataDTO {
    public Long id;
    public String username;
    public String password;
    public String role;

    public UserLoginDataDTO() {
    }

    private UserLoginDataDTO(Long p_id, String p_username, String p_password, String p_role) {
        id = p_id;
        username = p_username;
        password = p_password;
        role = p_role;
    }

    public static UserLoginDataDTO create(UserLoginData p_userLoginData) {
        return new UserLoginDataDTO(p_userLoginData.getId(),
                                    p_userLoginData.getUsername(),
                                    p_userLoginData.getPassword(),
                                    p_userLoginData.getRole());
    }

    public static UserLoginDataDTO createEmpty() {
        return new UserLoginDataDTO(0L, "", "", Roles.ROLE_ADMIN.name() + " / " + Roles.ROLE_EMPLOYEE.name());
    }

    public UserLoginData createEntityWithoutId() {
        return new UserLoginData()
                .setUsername(username)
                .setPassword(password)
                .setRole(role);
    }

    public UserLoginData createEntityWithId() {
        return createEntityWithoutId().setId(id);
    }
}
