package system.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by Pawel Polit
 */

@Entity
public class UserLoginData {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String role;

    public UserLoginData setId(Long p_id) {
        id = p_id;
        return this;
    }

    public UserLoginData setUsername(String p_username) {
        username = p_username;
        return this;
    }

    public UserLoginData setPassword(String p_password) {
        password = p_password;
        return this;
    }

    public UserLoginData setRole(String p_role) {
        role = p_role;
        return this;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getRole() {
        return role;
    }
}
