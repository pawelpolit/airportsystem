package system.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by Pawel Polit
 */

@Entity
public class Airport {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String city;

    public Airport setId(Long p_id) {
        id = p_id;
        return this;
    }

    public Airport setCity(String p_city) {
        city = p_city;
        return this;
    }

    public Long getId() {
        return id;
    }

    public String getCity() {
        return city;
    }
}
