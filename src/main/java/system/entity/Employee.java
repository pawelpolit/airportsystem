package system.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by Pawel Polit
 */

@Entity
public class Employee {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String surname;

    @Column(nullable = false)
    private Integer age;

    @Column
    private String pesel;

    @Column(nullable = false)
    private String position;

    public Employee setId(Long p_id) {
        id = p_id;
        return this;
    }

    public Employee setName(String p_name) {
        name = p_name;
        return this;
    }

    public Employee setSurname(String p_surname) {
        surname = p_surname;
        return this;
    }

    public Employee setAge(Integer p_age) {
        age = p_age;
        return this;
    }

    public Employee setPesel(String p_pesel) {
        pesel = p_pesel;
        return this;
    }

    public Employee setPosition(String p_position) {
        position = p_position;
        return this;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Integer getAge() {
        return age;
    }

    public String getPesel() {
        return pesel;
    }

    public String getPosition() {
        return position;
    }
}
