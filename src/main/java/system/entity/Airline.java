package system.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by Pawel Polit
 */

@Entity
public class Airline {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String name;

    public Airline setId(Long p_id) {
        id = p_id;
        return this;
    }

    public Airline setName(String p_name) {
        name = p_name;
        return this;
    }

    public String getName() {
        return name;
    }

    public Long getId() {
        return id;
    }
}
