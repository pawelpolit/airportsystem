package system.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

/**
 * Created by Pawel Polit
 */

@Entity
public class Flight {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "fromAirportId")
    private Airport fromAirport;

    @ManyToOne(optional = false)
    @JoinColumn(name = "toAirportId")
    private Airport toAirport;

    @Column(nullable = false)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime dateOfDeparture;

    @Column(nullable = false)
    private Integer flightTime;

    @ManyToOne(optional = false)
    @JoinColumn(name = "airlineId")
    private Airline airline;

    @Column(nullable = false)
    private Integer flightCost;

    @Column(nullable = false)
    private Integer maxNumberOfPassengers;

    public Flight setId(Long p_id) {
        id = p_id;
        return this;
    }

    public Flight setFromAirport(Airport p_fromAirport) {
        fromAirport = p_fromAirport;
        return this;
    }

    public Flight setToAirport(Airport p_toAirport) {
        toAirport = p_toAirport;
        return this;
    }

    public Flight setDateOfDeparture(DateTime p_dateOfDeparture) {
        dateOfDeparture = p_dateOfDeparture;
        return this;
    }

    public Flight setFlightTime(Integer p_flightTime) {
        flightTime = p_flightTime;
        return this;
    }

    public Flight setAirline(Airline p_airline) {
        airline = p_airline;
        return this;
    }

    public Flight setFlightCost(Integer p_flightCost) {
        flightCost = p_flightCost;
        return this;
    }

    public Flight setMaxNumberOfPassengers(Integer p_maxNumberOfPassengers) {
        maxNumberOfPassengers = p_maxNumberOfPassengers;
        return this;
    }

    public Long getId() {
        return id;
    }

    public Airport getFromAirport() {
        return fromAirport;
    }

    public Airport getToAirport() {
        return toAirport;
    }

    public DateTime getDateOfDeparture() {
        return dateOfDeparture;
    }

    public Integer getFlightTime() {
        return flightTime;
    }

    public Airline getAirline() {
        return airline;
    }

    public Integer getFlightCost() {
        return flightCost;
    }

    public Integer getMaxNumberOfPassengers() {
        return maxNumberOfPassengers;
    }
}
