package system.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by Pawel Polit
 */

@Entity
public class Reservation {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String personName;

    @Column(nullable = false)
    private String personSurname;

    @ManyToOne(optional = false)
    @JoinColumn(name = "flightId")
    private Flight flight;

    public Reservation setId(Long p_id) {
        id = p_id;
        return this;
    }

    public Reservation setPersonName(String p_personName) {
        personName = p_personName;
        return this;
    }

    public Reservation setPersonSurname(String p_personSurname) {
        personSurname = p_personSurname;
        return this;
    }

    public Reservation setFlight(Flight p_flight) {
        flight = p_flight;
        return this;
    }

    public Long getId() {
        return id;
    }

    public String getPersonName() {
        return personName;
    }

    public String getPersonSurname() {
        return personSurname;
    }

    public Flight getFlight() {
        return flight;
    }
}
