package system.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Created by Pawel Polit
 */

@Configuration
@ImportResource({"classpath:security-context.xml"})
@ComponentScan("system.security")
public class SpringSecurityConfig {

    public SpringSecurityConfig() {
        super();
    }
}
