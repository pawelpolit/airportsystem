package system.controller.utils;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by Pawel Polit
 */

public enum Utils {
    ;

    public static URI createURI(String p_context, String p_type) {
        try {
            return new URI(p_context + "/" + p_type);

        } catch(URISyntaxException p_e) {
            System.err.println("Error during creating URI\n");
            throw new IllegalArgumentException(p_e);
        }
    }

    public static URI createUri(String p_context, String p_type, Long p_id) {
        try {
            return new URI(p_context + "/" + p_type + "/id/" + p_id);

        } catch(URISyntaxException p_e) {
            System.err.println("Error during creating URI\n");
            throw new IllegalArgumentException(p_e);
        }
    }

    public static URI createUri(String p_context, String p_type, String p_suffix) {
        try {
            return new URI(p_context + "/" + p_type + "/" + p_suffix);

        } catch(URISyntaxException p_e) {
            System.err.println("Error during creating URI\n");
            throw new IllegalArgumentException(p_e);
        }
    }
}
