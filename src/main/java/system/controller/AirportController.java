package system.controller;

import system.controller.utils.Utils;
import system.dao.AirportDAO;
import system.dao.FlightDAO;
import system.dto.AirportDTO;
import system.entity.Airport;

import javax.servlet.ServletContext;
import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Pawel Polit
 */

@Controller
@RequestMapping("/airport")
public class AirportController {

    @Autowired
    private AirportDAO airportDAO;

    @Autowired
    private FlightDAO flightDAO;

    @Autowired
    private ServletContext context;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Map<String, Collection<URI>>> getAllAirports() {
        Multimap<String, URI> result = ArrayListMultimap.create();
        List<Airport> allAirports = Lists.newArrayList(airportDAO.findAll());

        allAirports.forEach(airport -> result.put(airport.getCity(),
                                                  Utils.createUri(context.getContextPath(), "airport", airport.getId())));

        return new ResponseEntity<>(result.asMap(), HttpStatus.OK);
    }

    @RequestMapping(value = "/id/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<AirportDTO> getAirportById(@PathVariable("id") Long p_id) {
        Airport airport = airportDAO.findOne(p_id);

        if(airport == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(AirportDTO.create(airport), HttpStatus.OK);
    }

    @RequestMapping(value = "/city/{city}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<URI>> getAirportsInCity(@PathVariable("city") String p_city) {
        return new ResponseEntity<>(airportDAO.findByCity(p_city).stream()
                                            .map(Airport::getId)
                                            .map(airportId -> Utils.createUri(context.getContextPath(), "airport", airportId))
                                            .collect(Collectors.toList()), HttpStatus.OK);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    @Secured("ROLE_ADMIN")
    public ResponseEntity<AirportDTO> createAirport(@RequestBody AirportDTO p_airportDTO) {
        Airport newAirport = airportDAO.save(p_airportDTO.createEntityWithoutId());
        return new ResponseEntity<>(AirportDTO.create(newAirport), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    @ResponseBody
    @Secured("ROLE_ADMIN")
    public ResponseEntity<AirportDTO> updateAirport(@RequestBody AirportDTO p_airportDTO) {
        Airport airport = p_airportDTO.createEntityWithId();

        if(!airportDAO.exists(airport.getId())) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        airportDAO.save(airport);
        return new ResponseEntity<>(AirportDTO.create(airport), HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/id/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    @Secured("ROLE_ADMIN")
    public ResponseEntity<String> deleteAirport(@PathVariable("id") Long p_id) {
        if(!airportDAO.exists(p_id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Airport airport = airportDAO.findOne(p_id);

        if(flightDAO.countByFromAirportOrToAirport(airport, airport) > 0) {
            return new ResponseEntity<>("There are some flights related with that airport - remove them first",
                                        HttpStatus.CONFLICT);
        }

        airportDAO.delete(p_id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
