package system.controller;

import system.controller.utils.Utils;
import system.dao.AirlineDAO;
import system.dao.FlightDAO;
import system.dto.AirlineDTO;
import system.entity.Airline;

import javax.servlet.ServletContext;
import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Pawel Polit
 */

@Controller
@RequestMapping("/airline")
public class AirlineController {

    @Autowired
    private AirlineDAO airlineDAO;

    @Autowired
    private FlightDAO flightDAO;

    @Autowired
    private ServletContext context;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Map<String, Collection<URI>>> getAllAirlines() {
        Multimap<String, URI> result = ArrayListMultimap.create();
        List<Airline> allAirlines = Lists.newArrayList(airlineDAO.findAll());

        allAirlines.forEach(airline -> result.put(airline.getName(),
                                                  Utils.createUri(context.getContextPath(), "airline", airline.getId())));

        return new ResponseEntity<>(result.asMap(), HttpStatus.OK);
    }

    @RequestMapping(value = "/id/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<AirlineDTO> getAirlineById(@PathVariable("id") Long p_id) {
        Airline airline = airlineDAO.findOne(p_id);

        if(airline == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(AirlineDTO.create(airline), HttpStatus.OK);
    }

    @RequestMapping(value = "/name/{name}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<URI>> getAirlineByName(@PathVariable("name") String p_name) {
        return new ResponseEntity<>(airlineDAO.findByName(p_name).stream()
                                            .map(Airline::getId)
                                            .map(airlineId -> Utils.createUri(context.getContextPath(), "airline", airlineId))
                                            .collect(Collectors.toList()), HttpStatus.OK);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    @Secured("ROLE_ADMIN")
    public ResponseEntity<AirlineDTO> createAirline(@RequestBody AirlineDTO p_airlineDTO) {
        Airline newAirline = airlineDAO.save(p_airlineDTO.createEntityWithoutId());
        return new ResponseEntity<>(AirlineDTO.create(newAirline), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    @ResponseBody
    @Secured("ROLE_ADMIN")
    public ResponseEntity<AirlineDTO> updateAirline(@RequestBody AirlineDTO p_airlineDTO) {
        Airline airline = p_airlineDTO.createEntityWithId();

        if(!airlineDAO.exists(airline.getId())) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        airlineDAO.save(airline);
        return new ResponseEntity<>(AirlineDTO.create(airline), HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/id/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    @Secured("ROLE_ADMIN")
    public ResponseEntity<String> deleteAirline(@PathVariable("id") Long p_id) {
        if(!airlineDAO.exists(p_id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if(flightDAO.countByAirline(airlineDAO.findOne(p_id)) > 0) {
            return new ResponseEntity<>("There are some flights related with that airline - remove them first",
                                        HttpStatus.CONFLICT);
        }

        airlineDAO.delete(p_id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
