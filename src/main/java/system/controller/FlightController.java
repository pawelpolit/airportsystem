package system.controller;

import system.controller.utils.Utils;
import system.dao.AirlineDAO;
import system.dao.AirportDAO;
import system.dao.FlightDAO;
import system.dao.ReservationDAO;
import system.dto.FlightDTO;
import system.entity.Airline;
import system.entity.Airport;
import system.entity.Flight;

import javax.servlet.ServletContext;
import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Pawel Polit
 */

@Controller
@RequestMapping("/flight")
public class FlightController {

    @Autowired
    private FlightDAO flightDAO;

    @Autowired
    private ReservationDAO reservationDAO;

    @Autowired
    private AirportDAO airportDAO;

    @Autowired
    private AirlineDAO airlineDAO;

    @Autowired
    private ServletContext context;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Map<List<String>, Collection<URI>>> getAllFlights() {
        Multimap<List<String>, URI> result = ArrayListMultimap.create();
        List<Flight> allFlights = Lists.newArrayList(flightDAO.findAll());

        allFlights.forEach(flight -> result.put(Lists.newArrayList(flight.getFromAirport().getCity(),
                                                                   flight.getToAirport().getCity()),
                                                Utils.createUri(context.getContextPath(), "flight", flight.getId())));

        return new ResponseEntity<>(result.asMap(), HttpStatus.OK);
    }

    @RequestMapping(value = "/id/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<FlightDTO> getFlightById(@PathVariable("id") Long p_id) {
        Flight flight = flightDAO.findOne(p_id);

        if(flight == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(FlightDTO.create(flight), HttpStatus.OK);
    }

    @RequestMapping(value = "/find", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<URI>> find(@RequestParam(value = "id", required = false, defaultValue = "0")
                                          Long p_id,
                                          @RequestParam(value = "from", required = false, defaultValue = "")
                                          String p_from,
                                          @RequestParam(value = "to", required = false, defaultValue = "")
                                          String p_to,
                                          @RequestParam(value = "dateOfDeparture", required = false, defaultValue = "")
                                          String p_dateOfDeparture,
                                          @RequestParam(value = "flightTime", required = false, defaultValue = "-1")
                                          Integer p_maxFlightTime,
                                          @RequestParam(value = "airline", required = false, defaultValue = "")
                                          String p_airline,
                                          @RequestParam(value = "cost", required = false, defaultValue = "-1")
                                          Integer p_maxCost,
                                          @RequestParam(value = "maxNumberOfPassengers", required = false, defaultValue = "-1")
                                          Integer p_maxNumberOfPassengers) {

        Stream<Flight> flights = Lists.newArrayList(flightDAO.findAll()).stream();

        if(p_id > 0) {
            flights = flights.filter(flight -> flight.getId().equals(p_id));
        }

        if(!"".equals(p_from)) {
            flights = flights.filter(flight -> flight.getFromAirport().getCity().equalsIgnoreCase(p_from));
        }

        if(!"".equals(p_to)) {
            flights = flights.filter(flight -> flight.getToAirport().getCity().equalsIgnoreCase(p_to));
        }

        if(!"".equals(p_dateOfDeparture)) {
            try {
                DateTime date = DateTime.parse(p_dateOfDeparture, DateTimeFormat.forPattern("dd-MM-yyyy"));
                flights = flights.filter(flight -> flight.getDateOfDeparture().getYear() == date.getYear() &&
                        flight.getDateOfDeparture().getMonthOfYear() == date.getMonthOfYear() &&
                        flight.getDateOfDeparture().getDayOfMonth() == date.getDayOfMonth());
            } catch(IllegalArgumentException p_e) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

        }

        if(p_maxFlightTime != -1) {
            flights = flights.filter(flight -> flight.getFlightTime() <= p_maxFlightTime);
        }

        if(!"".equals(p_airline)) {
            flights = flights.filter(flight -> flight.getAirline().getName().equalsIgnoreCase(p_airline));
        }

        if(p_maxCost != -1) {
            flights = flights.filter(flight -> flight.getFlightCost() <= p_maxCost);
        }

        if(p_maxNumberOfPassengers != -1) {
            flights = flights.filter(flight -> flight.getMaxNumberOfPassengers() <= p_maxNumberOfPassengers);
        }

        return new ResponseEntity<>(flights.map(flight -> Utils.createUri(context.getContextPath(), "flight", flight.getId()))
                                            .collect(Collectors.toList()), HttpStatus.OK);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    @Secured("ROLE_ADMIN")
    public ResponseEntity<FlightDTO> createFlight(@RequestBody FlightDTO p_flightDTO) {
        Flight newFlight = p_flightDTO.createEntityWithoutId();

        Airport fromAirport = airportDAO.findOne(newFlight.getFromAirport().getId());
        Airport toAirport = airportDAO.findOne(newFlight.getToAirport().getId());
        Airline airline = airlineDAO.findOne(newFlight.getAirline().getId());

        if(fromAirport == null || toAirport == null || airline == null ||
                !fromAirport.getCity().equals(newFlight.getFromAirport().getCity()) ||
                !toAirport.getCity().equals(newFlight.getToAirport().getCity()) ||
                !airline.getName().equals(newFlight.getAirline().getName())) {

            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        newFlight = flightDAO.save(newFlight);

        return new ResponseEntity<>(FlightDTO.create(newFlight), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    @ResponseBody
    @Secured("ROLE_ADMIN")
    public ResponseEntity<FlightDTO> updateFlight(@RequestBody FlightDTO p_flightDTO) {
        Flight flight = p_flightDTO.createEntityWithId();

        Airport fromAirport = airportDAO.findOne(flight.getFromAirport().getId());
        Airport toAirport = airportDAO.findOne(flight.getToAirport().getId());
        Airline airline = airlineDAO.findOne(flight.getAirline().getId());

        if(!flightDAO.exists(flight.getId()) || fromAirport == null || toAirport == null || airline == null ||
                !fromAirport.getCity().equals(flight.getFromAirport().getCity()) ||
                !toAirport.getCity().equals(flight.getToAirport().getCity()) ||
                !airline.getName().equals(flight.getAirline().getName())) {

            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        flightDAO.save(flight);
        return new ResponseEntity<>(FlightDTO.create(flight), HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/id/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    @Secured("ROLE_ADMIN")
    public ResponseEntity<String> deleteFlight(@PathVariable("id") Long p_id) {
        if(!flightDAO.exists(p_id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if(reservationDAO.countByFlight(flightDAO.findOne(p_id)) > 0) {
            return new ResponseEntity<>("There are some reservations related with that flight - remove them first",
                                        HttpStatus.CONFLICT);
        }

        flightDAO.delete(p_id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
