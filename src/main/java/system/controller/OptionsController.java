package system.controller;

import system.controller.utils.Utils;
import system.dto.OptionsDTO;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Pawel Polit
 */

@Controller
public class OptionsController {

    private static final String GET = "GET";
    private static final String POST = "POST";
    private static final String PUT = "PUT";
    private static final String DELETE = "DELETE";

    private static final String AIRLINE = "airline";
    private static final String AIRPORT = "airport";
    private static final String EMPLOYEE = "employee";
    private static final String FLIGHT = "flight";
    private static final String RESERVATION = "reservation";
    private static final String USER = "user";

    @Autowired
    private ServletContext context;

    @RequestMapping(value = "/options", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<OptionsDTO> mainOptions() {
        return new ResponseEntity<>(OptionsDTO.create()
                                            .addOption(GET, Utils.createURI(context.getContextPath(), AIRLINE))
                                            .addOption(GET, Utils.createURI(context.getContextPath(), AIRPORT))
                                            .addOption(GET, Utils.createURI(context.getContextPath(), EMPLOYEE))
                                            .addOption(GET, Utils.createURI(context.getContextPath(), FLIGHT))
                                            .addOption(GET, Utils.createURI(context.getContextPath(), RESERVATION))
                                            .addOption(GET, Utils.createURI(context.getContextPath(), USER)),
                                    HttpStatus.OK);
    }

    @RequestMapping(value = "/airline", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<OptionsDTO> airlineOptions() {
        return new ResponseEntity<>(OptionsDTO.create()
                                            .addOption(GET, Utils.createUri(context.getContextPath(), AIRLINE, "all"))
                                            .addOption(GET, Utils.createUri(context.getContextPath(), AIRLINE, "id/(id)"))
                                            .addOption(GET, Utils.createUri(context.getContextPath(), AIRLINE, "name/(name)"))
                                            .addOption(POST, Utils.createUri(context.getContextPath(), AIRLINE, "create"))
                                            .addOption(PUT, Utils.createUri(context.getContextPath(), AIRLINE, "update"))
                                            .addOption(DELETE, Utils.createUri(context.getContextPath(), AIRLINE, "id/(id)")),
                                    HttpStatus.OK);
    }

    @RequestMapping(value = "/airport", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<OptionsDTO> airportOptions() {
        return new ResponseEntity<>(OptionsDTO.create()
                                            .addOption(GET, Utils.createUri(context.getContextPath(), AIRPORT, "all"))
                                            .addOption(GET, Utils.createUri(context.getContextPath(), AIRPORT, "id/(id)"))
                                            .addOption(GET, Utils.createUri(context.getContextPath(), AIRPORT, "city/(city)"))
                                            .addOption(POST, Utils.createUri(context.getContextPath(), AIRPORT, "create"))
                                            .addOption(PUT, Utils.createUri(context.getContextPath(), AIRPORT, "update"))
                                            .addOption(DELETE, Utils.createUri(context.getContextPath(), AIRPORT, "id/(id)")),
                                    HttpStatus.OK);
    }

    @RequestMapping(value = "/employee", method = RequestMethod.GET)
    @Secured("ROLE_ADMIN")
    @ResponseBody
    public ResponseEntity<OptionsDTO> employeeOptions() {
        return new ResponseEntity<>(OptionsDTO.create()
                                            .addOption(GET, Utils.createUri(context.getContextPath(), EMPLOYEE, "all"))
                                            .addOption(GET, Utils.createUri(context.getContextPath(), EMPLOYEE, "id/(id)"))
                                            .addOption(GET, Utils.createUri(context.getContextPath(), EMPLOYEE, "find?id=()&name=()&surname=()&age=()&pesel=()&position=()"))
                                            .addOption(POST, Utils.createUri(context.getContextPath(), EMPLOYEE, "create"))
                                            .addOption(PUT, Utils.createUri(context.getContextPath(), EMPLOYEE, "update"))
                                            .addOption(DELETE, Utils.createUri(context.getContextPath(), EMPLOYEE, "id/(id)")),
                                    HttpStatus.OK);
    }

    @RequestMapping(value = "/flight", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<OptionsDTO> flightOptions() {
        return new ResponseEntity<>(OptionsDTO.create()
                                            .addOption(GET, Utils.createUri(context.getContextPath(), FLIGHT, "all"))
                                            .addOption(GET, Utils.createUri(context.getContextPath(), FLIGHT, "id/(id)"))
                                            .addOption(GET, Utils.createUri(context.getContextPath(), FLIGHT, "find?id=()&from=()&to=()&dateOfDeparture=()&flightTime=()&airline=()&cost=()&maxNumberOfPassengers=()"))
                                            .addOption(POST, Utils.createUri(context.getContextPath(), FLIGHT, "create"))
                                            .addOption(PUT, Utils.createUri(context.getContextPath(), FLIGHT, "update"))
                                            .addOption(DELETE, Utils.createUri(context.getContextPath(), FLIGHT, "id/(id)")),
                                    HttpStatus.OK);
    }

    @RequestMapping(value = "/reservation", method = RequestMethod.GET)
    @Secured("ROLE_EMPLOYEE")
    @ResponseBody
    public ResponseEntity<OptionsDTO> reservationOptions() {
        return new ResponseEntity<>(OptionsDTO.create()
                                            .addOption(GET, Utils.createUri(context.getContextPath(), RESERVATION, "all"))
                                            .addOption(GET, Utils.createUri(context.getContextPath(), RESERVATION, "id/(id)"))
                                            .addOption(GET, Utils.createUri(context.getContextPath(), RESERVATION, "find?id=()&name=()&surname=()&flight=()"))
                                            .addOption(POST, Utils.createUri(context.getContextPath(), RESERVATION, "create"))
                                            .addOption(PUT, Utils.createUri(context.getContextPath(), RESERVATION, "update"))
                                            .addOption(DELETE, Utils.createUri(context.getContextPath(), RESERVATION, "id/(id)")),
                                    HttpStatus.OK);
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    @Secured("ROLE_ADMIN")
    @ResponseBody
    public ResponseEntity<OptionsDTO> userLoginDataOptions() {
        return new ResponseEntity<>(OptionsDTO.create()
                                            .addOption(GET, Utils.createUri(context.getContextPath(), USER, "all"))
                                            .addOption(GET, Utils.createUri(context.getContextPath(), USER, "form"))
                                            .addOption(POST, Utils.createUri(context.getContextPath(), USER, "create"))
                                            .addOption(PUT, Utils.createUri(context.getContextPath(), USER, "update"))
                                            .addOption(DELETE, Utils.createUri(context.getContextPath(), USER, "id/(id)")),
                                    HttpStatus.OK);
    }
}
