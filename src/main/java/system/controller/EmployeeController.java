package system.controller;

import system.controller.utils.Utils;
import system.dao.EmployeeDAO;
import system.dto.EmployeeDTO;
import system.entity.Employee;

import javax.servlet.ServletContext;
import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Pawel Polit
 */

@Controller
@RequestMapping("/employee")
@Secured("ROLE_ADMIN")
public class EmployeeController {

    @Autowired
    private EmployeeDAO employeeDAO;

    @Autowired
    private ServletContext context;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Map<String, Collection<URI>>> getAllEmployees() {
        Multimap<String, URI> result = ArrayListMultimap.create();
        List<Employee> allEmployees = Lists.newArrayList(employeeDAO.findAll());

        allEmployees.forEach(employee -> result.put(employee.getName() + " " + employee.getSurname(),
                                                    Utils.createUri(context.getContextPath(), "employee", employee.getId())));

        return new ResponseEntity<>(result.asMap(), HttpStatus.OK);
    }

    @RequestMapping(value = "/id/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<EmployeeDTO> getEmployeeById(@PathVariable("id") Long p_id) {
        Employee employee = employeeDAO.findOne(p_id);

        if(employee == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(EmployeeDTO.create(employee), HttpStatus.OK);
    }

    @RequestMapping(value = "/find", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<URI>> find(@RequestParam(value = "id", required = false, defaultValue = "0")
                                          Long p_id,
                                          @RequestParam(value = "name", required = false, defaultValue = "")
                                          String p_name,
                                          @RequestParam(value = "surname", required = false, defaultValue = "")
                                          String p_surname,
                                          @RequestParam(value = "age", required = false, defaultValue = "-1")
                                          Integer p_age,
                                          @RequestParam(value = "pesel", required = false, defaultValue = "")
                                          String p_pesel,
                                          @RequestParam(value = "position", required = false, defaultValue = "")
                                          String p_position) {

        Stream<Employee> employees = Lists.newArrayList(employeeDAO.findAll()).stream();

        if(p_id > 0) {
            employees = employees.filter(employee -> employee.getId().equals(p_id));
        }

        if(!"".equals(p_name)) {
            employees = employees.filter(employee -> employee.getName().equalsIgnoreCase(p_name));
        }

        if(!"".equals(p_surname)) {
            employees = employees.filter(employee -> employee.getSurname().equalsIgnoreCase(p_surname));
        }

        if(p_age > -1) {
            employees = employees.filter(employee -> employee.getAge().equals(p_age));
        }

        if(!"".equals(p_pesel)) {
            employees = employees.filter(employee -> employee.getPesel().equalsIgnoreCase(p_pesel));
        }

        if(!"".equals(p_position)) {
            employees = employees.filter(employee -> employee.getPosition().equalsIgnoreCase(p_position));
        }

        return new ResponseEntity<>(employees.map(employee -> Utils.createUri(context.getContextPath(),
                                                                              "employee",
                                                                              employee.getId()))
                                            .collect(Collectors.toList()), HttpStatus.OK);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<EmployeeDTO> createEmployee(@RequestBody EmployeeDTO p_employeeDTO) {
        Employee newEmployee = employeeDAO.save(p_employeeDTO.createEntityWithoutId());
        return new ResponseEntity<>(EmployeeDTO.create(newEmployee), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<EmployeeDTO> updateEmployee(@RequestBody EmployeeDTO p_employeeDTO) {
        Employee employee = p_employeeDTO.createEntityWithId();

        if(!employeeDAO.exists(employee.getId())) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        employee = employeeDAO.save(employee);
        return new ResponseEntity<>(EmployeeDTO.create(employee), HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/id/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity deleteEmployee(@PathVariable("id") Long p_id) {
        if(!employeeDAO.exists(p_id)) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        employeeDAO.delete(p_id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
