package system.controller;

import system.controller.utils.Utils;
import system.dao.UserLoginDataDAO;
import system.dto.UserLoginDataDTO;
import system.entity.UserLoginData;

import javax.servlet.ServletContext;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Pawel Polit
 */

@Controller
@RequestMapping("/user")
@Secured("ROLE_ADMIN")
public class UserLoginDataController {

    @Autowired
    private UserLoginDataDAO userLoginDataDAO;

    @Autowired
    private ServletContext context;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Map<List<String>, Collection<URI>>> getAllUsers() {
        Multimap<List<String>, URI> result = ArrayListMultimap.create();
        ArrayList<UserLoginData> allUsers = Lists.newArrayList(userLoginDataDAO.findAll());

        allUsers.forEach(user -> result.put(Lists.newArrayList(user.getUsername(), user.getRole()),
                                            Utils.createUri(context.getContextPath(), "user", user.getId())));

        return new ResponseEntity<>(result.asMap(), HttpStatus.OK);
    }

    @RequestMapping(value = "/form", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<UserLoginDataDTO> getRegistrationForm() {
        return new ResponseEntity<>(UserLoginDataDTO.createEmpty(), HttpStatus.OK);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<UserLoginDataDTO> createUser(@RequestBody UserLoginDataDTO p_userLoginDataDTO) {
        UserLoginData newUser = p_userLoginDataDTO.createEntityWithoutId();

        if(userLoginDataDAO.countByUsername(newUser.getUsername()) > 0) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        newUser = userLoginDataDAO.save(newUser);
        return new ResponseEntity<>(UserLoginDataDTO.create(newUser), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity updateUser(@RequestBody UserLoginDataDTO p_userLoginDataDTO) {
        UserLoginData user = p_userLoginDataDTO.createEntityWithId();

        if(!userLoginDataDAO.exists(user.getId())) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        if(userLoginDataDAO.countByUsername(user.getUsername()) > 0) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        userLoginDataDAO.save(user);
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/id/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity deleteUser(@PathVariable("id") Long p_id) {
        if(!userLoginDataDAO.exists(p_id)) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        userLoginDataDAO.delete(p_id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
