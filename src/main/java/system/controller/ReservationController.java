package system.controller;

import system.controller.utils.Utils;
import system.dao.FlightDAO;
import system.dao.ReservationDAO;
import system.dto.ReservationDTO;
import system.entity.Flight;
import system.entity.Reservation;

import javax.servlet.ServletContext;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.collect.Lists;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Pawel Polit
 */

@Controller
@RequestMapping("/reservation")
@Secured("ROLE_EMPLOYEE")
public class ReservationController {

    @Autowired
    private ReservationDAO reservationDAO;

    @Autowired
    private FlightDAO flightDAO;

    @Autowired
    private ServletContext context;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<URI>> getAllReservations() {
        return new ResponseEntity<>(Lists.newArrayList(reservationDAO.findAll())
                                            .stream()
                                            .map(reservation -> Utils.createUri(context.getContextPath(),
                                                                                "reservation",
                                                                                reservation.getId()))
                                            .collect(Collectors.toList()), HttpStatus.OK);
    }

    @RequestMapping(value = "/id/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<ReservationDTO> getReservationById(@PathVariable("id") Long p_id) {
        Reservation reservation = reservationDAO.findOne(p_id);

        if(reservation == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(ReservationDTO.create(reservation), HttpStatus.OK);
    }

    @RequestMapping(value = "/find", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<URI>> find(@RequestParam(value = "id", required = false, defaultValue = "0")
                                          Long p_id,
                                          @RequestParam(value = "name", required = false, defaultValue = "")
                                          String p_name,
                                          @RequestParam(value = "surname", required = false, defaultValue = "")
                                          String p_surname,
                                          @RequestParam(value = "flight", required = false, defaultValue = "0")
                                          Long p_flightId) {

        Stream<Reservation> reservations = Lists.newArrayList(reservationDAO.findAll()).stream();

        if(p_id > 0) {
            reservations = reservations.filter(reservation -> reservation.getId().equals(p_id));
        }

        if(!"".equals(p_name)) {
            reservations = reservations.filter(reservation -> reservation.getPersonName().equalsIgnoreCase(p_name));
        }

        if(!"".equals(p_surname)) {
            reservations = reservations.filter(reservation -> reservation.getPersonSurname().equalsIgnoreCase(p_surname));
        }

        if(p_flightId > 0) {
            reservations = reservations.filter(reservation -> reservation.getFlight().getId().equals(p_flightId));
        }

        return new ResponseEntity<>(reservations.map(reservation -> Utils.createUri(context.getContextPath(),
                                                                                    "reservation",
                                                                                    reservation.getId()))
                                            .collect(Collectors.toList()), HttpStatus.OK);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ReservationDTO> createReservation(@RequestBody ReservationDTO p_reservationDTO) {
        Flight flight = flightDAO.findOne(p_reservationDTO.flightId);

        if(flight == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        int numberOfReservedSeats = reservationDAO.countByFlight(flight);

        if(numberOfReservedSeats >= flight.getMaxNumberOfPassengers()) {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }

        Reservation newReservation = p_reservationDTO.createEntityWithoutId(flight);
        newReservation = reservationDAO.save(newReservation);

        return new ResponseEntity<>(ReservationDTO.create(newReservation), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<ReservationDTO> updateReservation(@RequestBody ReservationDTO p_reservationDTO) {
        Flight flight = flightDAO.findOne(p_reservationDTO.flightId);

        if(flight == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        int numberOfReservedSeats = reservationDAO.countByFlight(flight);

        if(numberOfReservedSeats >= flight.getMaxNumberOfPassengers()) {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }

        Reservation reservation = p_reservationDTO.createEntityWithId(flight);
        reservation = reservationDAO.save(reservation);

        return new ResponseEntity<>(ReservationDTO.create(reservation), HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "id/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity deleteReservation(@PathVariable("id") Long p_id) {
        if(!reservationDAO.exists(p_id)) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        reservationDAO.delete(p_id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
