package system.dao;

import system.entity.Airline;
import system.entity.Airport;
import system.entity.Flight;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by Pawel Polit
 */

public interface FlightDAO extends CrudRepository<Flight, Long> {
    int countByFromAirportOrToAirport(Airport p_fromAirport, Airport p_toAirport);

    int countByAirline(Airline p_airline);
}
