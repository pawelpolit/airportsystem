package system.dao;

import system.entity.Flight;
import system.entity.Reservation;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by Pawel Polit
 */

public interface ReservationDAO extends CrudRepository<Reservation, Long> {
    int countByFlight(Flight p_flight);
}
