package system.dao;

import system.entity.UserLoginData;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by Pawel Polit
 */

public interface UserLoginDataDAO extends CrudRepository<UserLoginData, Long> {
    int countByUsername(String p_username);

    UserLoginData findByUsername(String p_username);
}
