package system.dao;

import system.entity.Employee;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by Pawel Polit
 */

public interface EmployeeDAO extends CrudRepository<Employee, Long> {
}
