package system.dao;

import system.entity.Airport;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by Pawel Polit
 */

public interface AirportDAO extends CrudRepository<Airport, Long> {
    List<Airport> findByCity(String p_city);
}
