package system.dao;

import system.entity.Airline;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by Pawel Polit
 */

public interface AirlineDAO extends CrudRepository<Airline, Long> {
    List<Airline> findByName(String p_name);
}
